#!/bin/sh

# Extraction script for:
# Super Mario All-Stars (Europe).iso

# Outputs:
# - Super Mario All-Stars (Virtual Console) (Sound) (Unverified).smc

# Requires: wit python2 lzh8.py brrencode3.py snesrestore.py ucon64

romextract()
{
	dependency_wit          || return 1
	dependency_python2      || return 1
	dependency_snesrestore  || return 1
	dependency_ucon64       || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID" --files=+/files/content5/LZH8SVMP.*

	echo "Extracting ROMs ..."
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/lzh8.py" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/content5/LZH8SVMP.pcm" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/content5/SVMP.pcm"
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/lzh8.py" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/content5/LZH8SVMP.rom" \
		"$SCRIPTID/Super Mario All-Stars (Virtual Console) (Unverified).rom"

	# TODO: GoodTools hashes match files without uCON64 chk fix
	echo "Merging Super Mario All-Stars ..."
	"$PYTHON2_PATH" "$SCRIPTDIR/tools/snesrestore.py" \
		"$SCRIPTID/Super Mario All-Stars (Virtual Console) (Unverified).rom" \
		"$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/content5/SVMP.pcm" \
		"$SCRIPTID/Super Mario All-Stars (Virtual Console) (Sound) (Unverified).smc"
	echo "Regenerating checksum for Super Mario World ..."
	"$UCON64_PATH" --snes --chk --nbak -o="$SCRIPTID" \
		"$SCRIPTID/Super Mario All-Stars (Virtual Console) (Sound) (Unverified).smc" > /dev/null 2>&1

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
