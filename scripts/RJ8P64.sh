#!/bin/sh

# Extraction script for:
# Indiana Jones and the Staff of Kings (Europe) (En,Fr,De,Es,It).iso

# Outputs:
# - Indiana Jones and the Fate of Atlantis (CD/DOS/English)
# - Indiana Jones and the Fate of Atlantis (Floppy/DOS/French)
# - Indiana Jones and the Fate of Atlantis (Floppy/DOS/German)
# - Indiana Jones and the Fate of Atlantis (Floppy/DOS/Italian)
# - Indiana Jones and the Fate of Atlantis (Floppy/DOS/Spanish)

# Requires: wit

romextract()
{
	dependency_wit          || return 1

	echo "Extracting files from ISO ..."
	"$WIT_PATH" X "$FILE" "$ROMEXTRACT_TMPDIR/$SCRIPTID"  \
    --files=+/files/Fate/*/ATLANTIS* \
    --files=+/files/Fate/en/MONSTER.SOU

	echo "Moving files ..."
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/Fate/en" \
		"$SCRIPTID/English"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/Fate/fr" \
		"$SCRIPTID/French"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/Fate/ge" \
		"$SCRIPTID/German"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/Fate/it" \
		"$SCRIPTID/Italian"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/DATA/files/Fate/sp" \
		"$SCRIPTID/Spanish"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
