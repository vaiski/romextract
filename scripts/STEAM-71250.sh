#!/bin/sh

# Extraction script for:
# Sonic Adventure DX (Sonic Adventure DX.exe)

# Outputs:
# - Dr. Robotniks Mean Bean Machine (USA, Europe).gg
# - G-Sonic ~ Sonic Blast (World).gg
# - Sonic Chaos (USA, Europe).gg
# - Sonic Drift 2 (Japan, USA).gg
# - Sonic Drift (Japan) (En).gg
# - Sonic Labyrinth (World).gg
# - Sonic Spinball (USA, Europe).gg
# - Sonic & Tails 2 (Japan).gg
# - Sonic & Tails (Japan) (En).gg
# - Sonic The Hedgehog 2 (World).gg
# - Sonic The Hedgehog - Triple Trouble (USA, Europe).gg
# - Sonic The Hedgehog (World) (Rev 1).gg
# - Tails Adventures (World) (En,Ja).gg
# - Tails no Skypatrol (Japan).gg

# Requires: prstool

romextract ()
{
	dependency_prstool	|| return 1

	echo "Decompressing ROMs ..."
	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID"
	cp "$(dirname "$FILE")/system/"*.PRS "$ROMEXTRACT_TMPDIR/$SCRIPTID/"

	for rom in "$ROMEXTRACT_TMPDIR/$SCRIPTID"/*.PRS; do
		"$PRSTOOL_PATH" -x "$rom" "$rom"
	done

	echo "Moving ROMs ..."
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/G-SONIC.PRS" \
		"$SCRIPTID/G-Sonic ~ Sonic Blast (World).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/LABYLIN.PRS" \
		"$SCRIPTID/Sonic Labyrinth (World).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/MBMACHIN.PRS" \
		"$SCRIPTID/Dr. Robotnik's Mean Bean Machine (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/S-DRIFT2.PRS" \
		"$SCRIPTID/Sonic Drift 2 (Japan, USA).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SKYPAT.PRS" \
		"$SCRIPTID/Tails no Skypatrol (Japan).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SONIC2.PRS" \
		"$SCRIPTID/Sonic The Hedgehog 2 (World).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SONIC-CH.PRS" \
		"$SCRIPTID/Sonic Chaos (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SONICDRI.PRS" \
		"$SCRIPTID/Sonic Drift (Japan) (En).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SONIC.PRS" \
		"$SCRIPTID/Sonic The Hedgehog (World) (Rev 1).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SONICTAI.PRS" \
		"$SCRIPTID/Sonic & Tails (Japan) (En).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SONIC_TT.PRS" \
		"$SCRIPTID/Sonic & Tails 2 (Japan).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/SPINBALL.PRS" \
		"$SCRIPTID/Sonic Spinball (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/S-TAIL2.PRS" \
		"$SCRIPTID/Sonic The Hedgehog - Triple Trouble (USA, Europe).gg"
	mv -i "$ROMEXTRACT_TMPDIR/$SCRIPTID/TAILSADV.PRS" \
		"$SCRIPTID/Tails Adventures (World) (En,Ja).gg"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
