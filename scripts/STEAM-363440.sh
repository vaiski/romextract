#!/bin/sh

# Extraction script for:
# Mega Man Legacy Collection (Proteus.exe)

# Outputs:
# - Mega Man (Legacy Collection) (Unverified).nes
# - Mega Man 2 (Legacy Collection) (Unverified).nes
# - Mega Man 3 (Legacy Collection) (Unverified).nes
# - Mega Man 4 (Legacy Collection) (Unverified).nes
# - Mega Man 5 (Legacy Collection) (Unverified).nes
# - Mega Man 6 (Legacy Collection) (Unverified).nes
# - Rockman (Japan) (En).nes
# - Rockman 2 - Dr. Wily no Nazo (Japan).nes
# - Rockman 3 - Dr. Wily no Saigo! (Japan).nes
# - Rockman 4 - Aratanaru Yabou!! (Japan).nes
# - Rockman 5 - Blues no Wana! (Japan).nes
# - Rockman 6 - Shijou Saidai no Tatakai!! (Japan).nes

# Thanks to:
# - Alex Page (github.com/anpage) for the original Python script
# - u/thedisgruntledcactus for posting the original script on r/emulation

romextract()
{
	echo "Prefixing files with iNES headers ..."
	# Octal for Dash/POSIX compatibility
	printf '\516\105\123\32\10\0\41\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Mega Man (Legacy Collection) (Unverified).nes"
	printf '\516\105\123\32\20\0\20\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Mega Man 2 (Legacy Collection) (Unverified).nes"
	printf '\516\105\123\32\20\20\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Mega Man 3 (Legacy Collection) (Unverified).nes"
	printf '\516\105\123\32\40\0\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Mega Man 4 (Legacy Collection) (Unverified).nes"
	printf '\516\105\123\32\20\40\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Mega Man 5 (Legacy Collection) (Unverified).nes"
	printf '\516\105\123\32\40\0\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Mega Man 6 (Legacy Collection) (Unverified).nes"
	printf '\516\105\123\32\10\0\41\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Rockman (Japan) (En).nes"
	printf '\516\105\123\32\20\0\20\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Rockman 2 - Dr. Wily no Nazo (Japan).nes"
	printf '\516\105\123\32\20\20\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Rockman 3 - Dr. Wily no Saigo! (Japan).nes"
	printf '\516\105\123\32\40\0\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Rockman 4 - Aratanaru Yabou!! (Japan).nes"
	printf '\516\105\123\32\20\40\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Rockman 5 - Blues no Wana! (Japan).nes"
	printf '\516\105\123\32\40\0\100\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Rockman 6 - Shijou Saidai no Tatakai!! (Japan).nes"

	echo "Extracting ROMs from Proteus.exe ..."
	# tail for offset, head for ROM size
	tail -c +2813617 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Mega Man (Legacy Collection) (Unverified).nes"
	tail -c +585073 "$FILE" | head -c +262144 \
		>> "$SCRIPTID/Mega Man 2 (Legacy Collection) (Unverified).nes"
	tail -c +847281 "$FILE" | head -c +393216 \
		>> "$SCRIPTID/Mega Man 3 (Legacy Collection) (Unverified).nes"
	tail -c +1240561 "$FILE" | head -c +524288 \
		>> "$SCRIPTID/Mega Man 4 (Legacy Collection) (Unverified).nes"
	tail -c +1764913 "$FILE" | head -c +524288 \
		>> "$SCRIPTID/Mega Man 5 (Legacy Collection) (Unverified).nes"
	tail -c +2289265 "$FILE" | head -c +524288 \
		>> "$SCRIPTID/Mega Man 6 (Legacy Collection) (Unverified).nes"
	tail -c +5317169 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Rockman (Japan) (En).nes"
	tail -c +3088625 "$FILE" | head -c +262144 \
		>> "$SCRIPTID/Rockman 2 - Dr. Wily no Nazo (Japan).nes"
	tail -c +3350833 "$FILE" | head -c +393216 \
		>> "$SCRIPTID/Rockman 3 - Dr. Wily no Saigo! (Japan).nes"
	tail -c +3744113 "$FILE" | head -c +524288 \
		>> "$SCRIPTID/Rockman 4 - Aratanaru Yabou!! (Japan).nes"
	tail -c +4268465 "$FILE" | head -c +524288 \
		>> "$SCRIPTID/Rockman 5 - Blues no Wana! (Japan).nes"
	tail -c +4792817 "$FILE" | head -c +524288 \
		>> "$SCRIPTID/Rockman 6 - Shijou Saidai no Tatakai!! (Japan).nes"

	echo "Script $SCRIPTID.sh done"
}
