#!/bin/sh

# Extraction script for:
# Konami Collector's Series: Castlevania & Contra (cc.exe)

# Outputs:
# - "Castlevania (Konami Collector's Series) (Unverified).nes"
# - "Castlevania II - Simon's Quest (Konami Collector's Series) (Unverified).nes"
# - "Castlevania III - Dracula's Curse (Konami Collector's Series) (Unverified).nes"
# - "Contra (Konami Collector's Series) (Unverified).nes"
# - "Jackal (Konami Collector's Series) (Unverified).nes"
# - "Super C (Konami Collector's Series) (Unverified).nes"

romextract()
{
	echo "Prefixing files with iNES headers ..."
	for rom in \
		"Castlevania (Konami Collector's Series) (Unverified).nes" \
		"Castlevania II - Simon's Quest (Konami Collector's Series) (Unverified).nes" \
		"Castlevania III - Dracula's Curse (Konami Collector's Series) (Unverified).nes" \
		"Contra (Konami Collector's Series) (Unverified).nes" \
		"Jackal (Konami Collector's Series) (Unverified).nes" \
		"Super C (Konami Collector's Series) (Unverified).nes" \
	; do
		# tail for offset, head for header size
		tail -c +1041777 "$FILE" | head -c +16 > "$SCRIPTID/$rom"
	done

	echo "Extracting ROMs from cc.exe ..."
	# tail for offset, head for ROM size
	tail -c +124273 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Castlevania (Konami Collector's Series) (Unverified).nes"
	tail -c +255345 "$FILE" | head -c +262144 \
		>> "$SCRIPTID/Castlevania II - Simon's Quest (Konami Collector's Series) (Unverified).nes"
	tail -c +517489 "$FILE" | head -c +393216 \
		>> "$SCRIPTID/Castlevania III - Dracula's Curse (Konami Collector's Series) (Unverified).nes"
	tail -c +910705 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Contra (Konami Collector's Series) (Unverified).nes"
	tail -c +1041793 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Jackal (Konami Collector's Series) (Unverified).nes"
	tail -c +1172865 "$FILE" | head -c +262144 \
		>> "$SCRIPTID/Super C (Konami Collector's Series) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
