#!/bin/sh

# Extraction script for:
# The Disney Afternoon Collection (capcom_disney_afternoon.exe)

# Outputs:
# - Chip 'n Dale Rescue Rangers (Afternoon Collection) (Unverified).nes
# - Chip 'n Dale Rescue Rangers 2 (Afternoon Collection) (Unverified).nes
# - Darkwing Duck (Afternoon Collection) (Unverified).nes
# - Duck Tales (Afternoon Collection) (Unverified).nes
# - Duck Tales 2 (Afternoon Collection) (Unverified).nes
# - Tale Spin (Afternoon Collection) (Unverified).nes

romextract()
{
	echo "Prefixing files with iNES headers ..."
	# Octal for Dash/POSIX compatibility
	printf '\516\105\123\32\10\20\20\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Chip 'n Dale Rescue Rangers (Afternoon Collection) (Unverified).nes"
	printf '\516\105\123\32\10\20\20\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Chip 'n Dale Rescue Rangers 2 (Afternoon Collection) (Unverified).nes"
	printf '\516\105\123\32\10\20\20\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Darkwing Duck (Afternoon Collection) (Unverified).nes"
	printf '\516\105\123\32\10\0\41\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Duck Tales (Afternoon Collection) (Unverified).nes"
	printf '\516\105\123\32\10\0\41\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Duck Tales 2 (Afternoon Collection) (Unverified).nes"
	printf '\516\105\123\32\10\20\20\0\0\0\0\0\0\0\0\0' \
		> "$SCRIPTID/Tale Spin (Afternoon Collection) (Unverified).nes"

	echo "Extracting ROMs from capcom_disney_afternoon.exe ..."
	# tail for offset, head for ROM size
	tail -c +8335153 "$FILE" | head -c +262144 \
		>> "$SCRIPTID/Chip 'n Dale Rescue Rangers (Afternoon Collection) (Unverified).nes"
	tail -c +8597297 "$FILE" | head -c +262144 \
		>> "$SCRIPTID/Chip 'n Dale Rescue Rangers 2 (Afternoon Collection) (Unverified).nes"
	tail -c +7941937 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Darkwing Duck (Afternoon Collection) (Unverified).nes"
	tail -c +7810865 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Darkwing Duck (Afternoon Collection) (Unverified).nes"
	tail -c +8073009 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Duck Tales (Afternoon Collection) (Unverified).nes"
	tail -c +8204081 "$FILE" | head -c +131072 \
		>> "$SCRIPTID/Duck Tales 2 (Afternoon Collection) (Unverified).nes"
	tail -c +8859441 "$FILE" | head -c +262144 \
		>> "$SCRIPTID/Tale Spin (Afternoon Collection) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
