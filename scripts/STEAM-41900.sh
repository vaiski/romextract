#!/bin/sh

# Extraction script for:
# The Bard's Tale (Windows/Linux)

# Outputs:
# - The Bard's Tale - Tales of the Unknown (Apple IIGS)
# - The Bard's Tale II - The Destiny Knight (Apple IIGS)
# - The Bard's Tale III - The Thief of Fate (Apple II)
# - Apple IIGS ROM 01

romextract()
{

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID" || return 1

	echo "Decompressing files ..."
	unzip -uo -P 02iibt112 "$(dirname "$FILE")/res/iibt1.bin" \
		"*.2mg" -d "$ROMEXTRACT_TMPDIR/$SCRIPTID" > /dev/null

	unzip -uo -P 02iibt212 "$(dirname "$FILE")/res/iibt2.bin" \
		"*.2mg" -d "$ROMEXTRACT_TMPDIR/$SCRIPTID" > /dev/null

	unzip -uo -P 02iibt312 "$(dirname "$FILE")/res/iibt3.bin" \
		"*.dsk" -d "$ROMEXTRACT_TMPDIR/$SCRIPTID" > /dev/null

	echo "Moving files ..."
	# The Bard's Tale
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt1d1.2mg" \
		"$SCRIPTID/Bard's Tale - Tales of the Unknown, The (Disk 1 of 2).2mg"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt1d2.2mg" \
		"$SCRIPTID/Bard's Tale - Tales of the Unknown, The (Disk 2 of 2)(Character).2mg"
	# The Bard's Tale II
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt2d1.2mg" \
		"$SCRIPTID/Bard's Tale II - The Destiny Knight, The (Disk 1 of 2).2mg"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt2d2.2mg" \
		"$SCRIPTID/Bard's Tale II - The Destiny Knight, The (Disk 2 of 2)(Character).2mg"
	# The Bard's Tale III
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt3d1.dsk" \
		"$SCRIPTID/Bard's Tale III - The Thief of Fate, The (Disk 1 of 4).dsk"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt3d2.dsk" \
		"$SCRIPTID/Bard's Tale III - The Thief of Fate, The (Disk 2 of 4)(Character).dsk"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt3d3.dsk" \
		"$SCRIPTID/Bard's Tale III - The Thief of Fate, The (Disk 3 of 4)(Dungeon 1).dsk"
	mv "$ROMEXTRACT_TMPDIR/$SCRIPTID/bt3d4.dsk" \
		"$SCRIPTID/Bard's Tale III - The Thief of Fate, The (Disk 4 of 4)(Dungeon 2).dsk"

	echo "Copy system file ..."
	cp "$(dirname "$FILE")/res/sys/bttricode" \
		"$SCRIPTID/APPLE2GS.ROM"

	echo "Cleaning up ..."
	chmod 0644 "$SCRIPTID/"*
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"
}
