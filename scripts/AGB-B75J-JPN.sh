#!/bin/sh

# Extraction script for:
# Hudson Best Collection Vol. 5 - Shooting Collection (Japan)

# Outputs:
# - Star Force (Japan) (Hudson Best Collection) (Unverified).nes
# - Star Soldier (Japan) (Hudson Best Collection) (Unverified).nes
# - Hector '87 (Japan) (Hudson Best Collection) (Unverified).nes

romextract()
{
	echo "Extracting ROMs ..."
	# tail for offset, head for ROM size
	tail -c +167273 "$FILE" | head -c +24592 \
		> "$SCRIPTID/Star Force (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +191865 "$FILE" | head -c +65552 \
		> "$SCRIPTID/Star Soldier (Japan) (Hudson Best Collection) (Unverified).nes"
	tail -c +257417 "$FILE" | head -c +131088 \
		> "$SCRIPTID/Hector '87 (Japan) (Hudson Best Collection) (Unverified).nes"

	echo "Script $SCRIPTID.sh done"
}
