#!/bin/sh

# Extraction script for:
# SNK 40th Anniversary Collection

# Outputs:
# - Alpha Mission (USA).nes
# - ASO - Armored Scrum Object (Japan) (En).nes
# - Athena (Japan).nes
# - Athena (USA).nes
# - Crystalis (Japan) (SNK 40th Anniversary Collection) (Unverified).nes
# - Crystalis (SNK 40th Anniversary Collection) (Unverified).nes
# - Datsugoku (Japan).nes
# - Guerrilla War (USA).nes
# - Guevara (Japan).nes
# - Ikari II - Dogosoken (Japan).nes
# - Ikari III (Japan).nes
# - Ikari III - The Rescue (USA).nes
# - Ikari (Japan).nes
# - Ikari Warriors II - Victory Road (USA).nes
# - Ikari Warriors (USA) (Rev 1).nes
# - Iron Tank (Japan) (SNK 40th Anniversary Collection) (Unverified).nes
# - Iron Tank - The Invasion of Normandy (USA).nes
# - P.O.W. - Prisoners of War (USA).nes
# - bbustersj.zip (Beast Busters (Japan, Version 2, 3 Player) / MAME 0.206)
# - bbustersu.zip (Beast Busters (US, Version 3) / MAME 0.206)
# - bbusters.zip (Beast Busters (World) / MAME 0.206)
# - btlfield.zip (Battle Field (Japan) / MAME 0.206)
# - chopperb.zip (Chopper I (US) / MAME 0.206)
# - fantasyj.zip (Fantasy (Japan) / MAME 0.206)
# - fantasyu.zip (Fantasy (US) / MAME 0.206)
# - gensitou.zip (Genshi-Tou 1930's / MAME 0.206)
# - ikari3j.zip (Ikari Three (Japan, Rotary Joystick) / MAME 0.206)
# - ikari3k.zip (Ikari Three (Korea, 8-Way Joystick) / MAME 0.206)
# - ikari3u.zip (Ikari III - The Rescue (US, Rotary Joystick) / MAME 0.206)
# - ikari3.zip (Ikari III - The Rescue (World version 1, 8-Way Joystick) / MAME 0.206)
# - joyfulr.zip (Joyful Road (Japan) / MAME 0.206)
# - legofair.zip (Koukuu Kihei Monogatari - The Legend of Air Cavalry (Japan) / MAME 0.206)
# - mnchmobl.zip (Munch Mobile (US) / MAME 0.206)
# - ozmawars.zip (Ozma Wars (set 1) / MAME 0.206)
# - powj.zip (Datsugoku - Prisoners of War (Japan) / MAME 0.206)
# - pow.zip (P.O.W. - Prisoners of War (US version 1) / MAME 0.206)
# - prehisleu.zip (Prehistoric Isle in 1930 (US) / MAME 0.206)
# - prehisle.zip (Prehistoric Isle in 1930 (World) / MAME 0.206)
# - sasuke.zip (Sasuke vs. Commander / MAME 0.206)
# - searcharj.zip (SAR - Search And Rescue (Japan version 3) / MAME 0.206)
# - searcharu.zip (SAR - Search And Rescue (US) / MAME 0.206)
# - searchar.zip (SAR - Search And Rescue (World) / MAME 0.206)
# - streetsm1.zip (Street Smart (US version 1) / MAME 0.206)
# - streetsmj.zip (Street Smart (Japan version 1) / MAME 0.206)
# - streetsmw.zip (Street Smart (World version 1) / MAME 0.206)
# - streetsm.zip (Street Smart (US version 2) / MAME 0.206)
# - timesold.zip (Time Soldiers (US Rev 3) / MAME 0.206)
# - vanguardc.zip (Vanguard (Centuri) / MAME 0.206)
# - vanguardj.zip (Vanguard (Japan) / MAME 0.206)
# - vanguard.zip (Vanguard (SNK) / MAME 0.206)

load16_byte() {
  xxd -p -c2|sed -e "s/\(..\)\(..\)/\\$1/"|xxd -r -p
}

romextract()
{
	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID" || return 1

	if [ -f "$(dirname "$FILE")/Bundle/bundleMain.mbundle" ]; then
	 FILEBUNDLE="$(dirname "$FILE")/Bundle/bundleMain.mbundle"
	else
		echo "Could not find bundleMain.mbundle"
		return 1
	fi

	echo "Extracting needed files from bundleMain.mbundle ..."

	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.gfx1"                    bs=65536   skip=140538074  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj.maincpu"                 bs=262144  skip=152707694  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.z80"         bs=65536   skip=187001988  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.snk6502"                  bs=4096    skip=17074270   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.gfx1"                      bs=65536   skip=208205686  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.maincpu"                 bs=262144  skip=212680688  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj.maincpu"                      bs=262144  skip=293505487  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j.maincpu"                   bs=262144  skip=295502227  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.proms"                    bs=64      skip=347060986  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.maincpu"                       bs=262144  skip=361449617  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.plds"                          bs=204     skip=367802639  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu"                 bs=32768   skip=379279241  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u.maincpu"                   bs=262144  skip=404121204  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech"                   bs=22528   skip=416759430  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.w.68k"       bs=262144  skip=435584335  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.maincpu"                   bs=262144  skip=475673545  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"                      bs=131072  skip=482605397  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.map"      bs=65536   skip=500373291  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.u.68k"       bs=262144  skip=505531143  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.j.68k"       bs=262144  skip=530868731  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.upd"                      bs=131072  skip=596361190  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw.maincpu"                 bs=262144  skip=598083564  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.sprites.rom" bs=655360  skip=647720236  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.soundcpu"                      bs=65536   skip=87304517   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.soundcpu"                   bs=65536   skip=88131877   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu"                 bs=32768   skip=687863183  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu"                  bs=32768   skip=693775116  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.upd"                           bs=65536   skip=698135219  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx1"                       bs=65536   skip=93968193   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx1"                     bs=65536   skip=94391091   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.upd"                        bs=131072  skip=754095905  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2"                       bs=3407872 skip=98751342   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.gfx1"                     bs=4096    skip=70509      iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2"                     bs=3670016 skip=102512348  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.fg.rom"      bs=262144  skip=904380241  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.rom"      bs=262144  skip=950954188  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.samples"     bs=131072  skip=952093548  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.soundcpu"                 bs=65536   skip=953394451  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.maincpu"                    bs=262144  skip=962259705  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.w.alpha.rom" bs=32768   skip=963354851  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.maincpu"                  bs=262144  skip=979543384  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx1"                          bs=65536   skip=1009965196 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2"                          bs=2097152 skip=1011257704 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/IronTank.nes"                      bs=262160  skip=128079593  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ASO_jp.nes"                        bs=65552   skip=166581940  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/GuerrillaWar_jp.nes"               bs=262160  skip=177473971  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Crystalis.nes"                     bs=393232  skip=196991189  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari_jp.nes"                      bs=131088  skip=20029116   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Athena_jp.nes"                     bs=131088  skip=234606294  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/POW_jp.nes"                        bs=262160  skip=250688812  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari2_jp.nes"                     bs=262160  skip=278454754  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari2.nes"                        bs=262160  skip=353430127  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/GuerrillaWar.nes"                  bs=262160  skip=428642800  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari3_jp.nes"                     bs=262160  skip=468537441  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ASO.nes"                           bs=65552   skip=490174910  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/POW.nes"                           bs=262160  skip=506695829  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Athena.nes"                        bs=131088  skip=553836672  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Crystalis_jp.nes"                  bs=393232  skip=573861705  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/IronTank_jp.nes"                   bs=262160  skip=665382968  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari3.nes"                        bs=262160  skip=776154033  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari.nes"                         bs=131088  skip=803667288  iflag=skip_bytes count=1

	if [ -f "$(dirname "$FILE")/Bundle/bundleDLC1.mbundle" ]; then
		FILEBUNDLE="$(dirname "$FILE")/Bundle/bundleDLC1.mbundle"
	else
		echo "Could not find bundleDLC1.mbundle"
		return 1
	fi

	echo "Extracting needed files from bundleDLC1.mbundle ..."

	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.maincpu"     bs=262144  skip=1527570  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx4"         bs=524288  skip=1789720  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.maincpu"      bs=262144  skip=187023   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx1"         bs=131072  skip=2314014  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx1"         bs=65536   skip=2586752  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.ymsnd"        bs=524288  skip=2652294  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.user1"        bs=262144  skip=3176588  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.ymsnd.deltat" bs=524288  skip=3438738  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj.maincpu"     bs=524288  skip=42260438 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx5"         bs=524288  skip=43509865 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2"         bs=2097152 skip=44034159 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table"  bs=65536   skip=46131317 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2"         bs=3670016 skip=46196859 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj.maincpu"     bs=262144  skip=64744846 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.eeprom"       bs=256     skip=65006996 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.upd"          bs=131072  skip=65007258 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.user1"       bs=262144  skip=66031108 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.maincpu"      bs=524288  skip=66293258 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.soundcpu"     bs=65536   skip=66817552 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3"         bs=2097152 skip=66955340 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.audiocpu"     bs=65536   skip=917535   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu.maincpu"     bs=524288  skip=983077   iflag=skip_bytes count=1

	if [ -f "$(dirname "$FILE")/Bundle/bundlePatch1.mbundle" ]; then
		FILEBUNDLE="$(dirname "$FILE")/Bundle/bundlePatch1.mbundle"
	else
		echo "Could not find bundlePatch1.mbundle"
		return 1
	fi

	echo "Extracting needed files from bundlePatch1.mbundle ..."

	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu"           bs=36864   skip=146379596 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.ym2"                bs=65536   skip=146416466 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles"           bs=262144  skip=146629476 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.snk6502"           bs=6144    skip=147415920 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.proms"              bs=3072    skip=149938294 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.proms"               bs=32      skip=150774986 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu"           bs=36864   skip=150775024 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.gfx1.rom"    bs=65536   skip=150811894 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.audiocpu"           bs=65536   skip=150990236 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.proms"             bs=64      skip=151055778 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.maincpu"            bs=16384   skip=152404749 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.gfx3"              bs=24576   skip=15449952  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.pal"                bs=2048    skip=15564658  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu"             bs=20480   skip=15566712  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.proms"              bs=256     skip=15751050  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.u.gfx1.rom"    bs=65536   skip=15751312  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx1"               bs=8192    skip=165824910 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sub"                bs=65536   skip=166126741 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair.sub"               bs=65536   skip=166200481 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx2"               bs=8192    skip=166326964 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.tx_tiles"           bs=32768   skip=166948657 iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.snk6502"             bs=2048    skip=18608931  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.plds"               bs=260     skip=19165279  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.maincpu"           bs=20480   skip=19165545  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.proms"             bs=64      skip=19221088  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom"      bs=2097152 skip=19221158  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.maincpu"            bs=65536   skip=22559118  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx3"               bs=24576   skip=2495642   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.68k"         bs=262144  skip=63985336  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.gfx1"              bs=8192    skip=64338964  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.gfx3.patched.vrom" bs=65536   skip=64347162  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.audiocpu"           bs=8192    skip=65553306  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair.maincpu"           bs=65536   skip=65561504  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.maincpu"           bs=16384   skip=65627046  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.speech"            bs=6144    skip=67549650  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles"         bs=524288  skip=6812012   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles"         bs=131072  skip=68760649  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.gfx1"                bs=4096    skip=7616392   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx4"               bs=8192    skip=7620494   iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.3.68k"         bs=262144  skip=93810922  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.z80"           bs=196608  skip=94073072  iflag=skip_bytes count=1
	dd status=none if="$FILEBUNDLE" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/moonbase.proms"             bs=2048    skip=94490846  iflag=skip_bytes count=1

	echo "Extracting and compressing MAME ROMs ..."

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-3.k10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-5.k12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.maincpu" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-2.k8"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.maincpu" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-4.k11"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.audiocpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-1.e6" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-10.l9" bs=131072 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f11.m16" bs=524288 skip=0 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f12.m13" bs=524288 skip=524288 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f13.m12" bs=524288 skip=1048576 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f14.m11" bs=524288 skip=1572864 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f21.l10" bs=524288 skip=0 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f22.l12" bs=524288 skip=524288 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f23.l13" bs=524288 skip=1048576 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-f24.l15" bs=524288 skip=1572864 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx4" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-back1.m4" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx5" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-back2.m6" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-6.e7" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-7.h7" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-8.a14" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-9.c14" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.ymsnd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-pcma.l5" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.ymsnd.deltat" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bb-pcmb.l3" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.eeprom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/bbusters-eeprom.bin" bs=256 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-ver3-u3.k10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-ver3-u5.k12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu.maincpu" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-2.k8"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu.maincpu" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-4.k11"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.audiocpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-1.e6" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-10.l9" bs=131072 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f11.m16" bs=524288 skip=0 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f12.m13" bs=524288 skip=524288 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f13.m12" bs=524288 skip=1048576 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f14.m11" bs=524288 skip=1572864 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f21.l10" bs=524288 skip=0 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f22.l12" bs=524288 skip=524288 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f23.l13" bs=524288 skip=1048576 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-f24.l15" bs=524288 skip=1572864 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx4" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-back1.m4" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx5" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-back2.m6" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-6.e7" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-7.h7" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-8.a14" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-9.c14" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.ymsnd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bb-pcma.l5" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.eeprom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/bbusters-eeprom.bin" bs=256 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb3_ver2_j3.k10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb5_ver2_j3.k12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj.maincpu" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-2.k8"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj.maincpu" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-4.k11"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.audiocpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-1.e6" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-10.l9" bs=131072 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f11.m16" bs=524288 skip=0 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f12.m13" bs=524288 skip=524288 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f13.m12" bs=524288 skip=1048576 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f14.m11" bs=524288 skip=1572864 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f21.l10" bs=524288 skip=0 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f22.l12" bs=524288 skip=524288 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f23.l13" bs=524288 skip=1048576 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-f24.l15" bs=524288 skip=1572864 iflag=skip_bytes count=1 conv=swab
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx4" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-back1.m4" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.gfx5" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-back2.m6" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-6.e7" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-7.h7" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-8.a14" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.scale_table" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-9.c14" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.ymsnd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-pcma.l5" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.ymsnd.deltat" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bb-pcmb.l3" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.eeprom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/bbusters-eeprom.bin" bs=256 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_01.8g"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sub" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_04.6g"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.audiocpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_03.3d"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/k1.9w" bs=1024 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/k3.9u" bs=1024 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/k2.9v" bs=1024 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.tx_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_05.8p"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_10.8y" bs=65536 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_11.8z" bs=65536 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_12.8ab" bs=65536 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_13.8ac" bs=65536 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_09.3k" bs=32768 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_08.3l" bs=32768 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_07.3n" bs=32768 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_06.3p" bs=32768 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_18.3ab" bs=65536 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_19.2ad" bs=65536 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_20.3y" bs=65536 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_21.3aa" bs=65536 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_14.3v" bs=65536 count=1 skip=4
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_15.3x" bs=65536 count=1 skip=5
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_16.3s" bs=65536 count=1 skip=6
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_17.3t" bs=65536 count=1 skip=7
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.ym2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/kk_2.3j"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.plds" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/pal16r6b.2c"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/up03_m4.rom"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair.sub" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/up03_m8.rom"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.audiocpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_03.3d"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/up03_k1.rom" bs=1024 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/up03_l1.rom" bs=1024 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/up03_k2.rom" bs=1024 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.tx_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_05.8p"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_10.8y" bs=65536 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_11.8z" bs=65536 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_12.8ab" bs=65536 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.bg_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_13.8ac" bs=65536 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_09.3k" bs=32768 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_08.3l" bs=32768 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_07.3n" bs=32768 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp16_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_06.3p" bs=32768 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_18.3ab" bs=65536 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_19.2ad" bs=65536 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_20.3y" bs=65536 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_21.3aa" bs=65536 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_14.3v" bs=65536 count=1 skip=4
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_15.3x" bs=65536 count=1 skip=5
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_16.3s" bs=65536 count=1 skip=6
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.sp32_tiles" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_17.3t" bs=65536 count=1 skip=7
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.ym2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/kk_2.3j"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/chopper.plds" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/pal16r6b.2c"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic12.cpu" bs=4096 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic07.cpu" bs=4096 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic08.cpu" bs=4096 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic09.cpu" bs=4096 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic10.cpu" bs=4096 count=1 skip=4
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic14.cpu" bs=4096 count=1 skip=5
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic15.cpu" bs=4096 count=1 skip=6
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic16.cpu" bs=4096 count=1 skip=7
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/ic17.cpu" bs=4096 count=1 skip=8
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs10ic50.bin" bs=4096 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs11ic51.bin" bs=4096 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fantasy.ic7" bs=32 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fantasy.ic6" bs=32 skip=32 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs_b_51.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs_a_52.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs_c_53.bin" bs=2048 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs_d_7.bin" bs=2048 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs_e_8.bin" bs=2048 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/fs_f_11.bin" bs=2048 count=1 skip=2

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs5jic12.bin" bs=4096 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs1jic7.bin" bs=4096 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs2jic8.bin" bs=4096 count=1 skip=2
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs3jic9.bin" bs=4096 count=1 skip=3
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs4jic10.bin" bs=4096 count=1 skip=4
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs6jic14.bin" bs=4096 count=1 skip=5
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs7jic15.bin" bs=4096 count=1 skip=6
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs8jic16.bin" bs=4096 count=1 skip=7
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs9jic17.bin" bs=4096 count=1 skip=8
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs10ic50.bin" bs=4096 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs11ic51.bin" bs=4096 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/prom-8.bpr" bs=32 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/prom-7.bpr" bs=32 skip=32 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs_b_51.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs_a_52.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs_c_53.bin" bs=2048 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs_d_7.bin" bs=2048 count=1 skip=0
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs_e_8.bin" bs=2048 count=1 skip=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/fs_f_11.bin" bs=2048 count=1 skip=2

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.maincpu"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-2-ver1.c10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.maincpu"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-3-ver1.c9"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-1.c8"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-4.c12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-5.16d" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-7.16l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-8.16m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-23.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-13.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-22.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-12.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=4|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-21.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=4|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-11.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=6|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-20.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=6|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-10.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-19.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-9.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=16|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-14.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=16|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-24.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=18|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-15.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=18|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-25.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=20|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-16.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=20|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-26.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=22|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-17.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=22|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-27.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-18.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-28.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/ik3-6.18e" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u.maincpu"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-2.c10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u.maincpu"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-3.c9"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-1.c8"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-4.c12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-5.15d" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-7.16l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-8.16m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-23.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-13.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-22.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-12.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=4|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-21.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=4|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-11.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=6|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-20.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=6|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-10.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-19.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-9.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=16|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-14.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=16|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-24.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=18|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-15.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=18|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-25.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=20|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-16.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=20|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-26.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=22|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-17.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=22|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-27.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-18.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-28.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/ik3-6.18e" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j.maincpu"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-2-j.c10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j.maincpu"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-3-j.c9"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-1.c8"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-4.c12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-5.16d" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-7.16l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-8.16m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-23.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-13.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-22.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-12.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=4|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-21.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=4|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-11.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=6|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-20.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=6|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-10.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-19.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-9.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=16|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-14.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=16|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-24.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=18|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-15.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=18|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-25.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=20|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-16.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=20|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-26.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=22|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-17.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=22|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-27.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-18.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-28.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/ik3-6.18e" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.maincpu"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-2k.c10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.maincpu"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-3k.c9"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-1.c8"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.user1"|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-4.c12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-5.16d" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-7k.16l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-8k.16m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=8 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ikari-880d_t53.d2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=8 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ikari-880c_t54.c2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik12.d1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=8|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik11.c1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=8 skip=16|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ikari-880d_t52.b2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=8 skip=16|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ikari-880c_t51.a2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik10.b1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.gfx2" bs=131072 count=2 skip=24|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik9.a1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/ik3-6.18e" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/m1j.10e" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/m2j.10d" bs=8192 skip=8192 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.audiocpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/mu.2j" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/s1.10a" bs=4096 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/s2.10b" bs=4096 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/b1.2c" bs=4096 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/b2.2b" bs=4096 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/f1j.1g" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/f2j.3g" bs=8192 skip=8192 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/f3j.5g" bs=8192 skip=16384 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx4" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/h" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/a2001.clr" bs=256 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/m1.10e" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/m2.10d" bs=8192 skip=8192 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.audiocpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/mu.2j" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/s1.10a" bs=4096 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/s2.10b" bs=4096 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/b1.2c" bs=4096 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/b2.2b" bs=4096 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/f1.1g" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/f2.3g" bs=8192 skip=8192 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.gfx3" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/f3.5g" bs=8192 skip=16384 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.gfx4" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/h" bs=8192 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/a2001.clr" bs=256 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/mw01" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/mw02" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/mw03" bs=2048 skip=4096 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/mw04" bs=2048 skip=6144 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/mw05" bs=2048 skip=16384 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/mw06" bs=2048 skip=18432 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/moonbase.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/01.1" bs=1024 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/moonbase.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/02.2" bs=1024 skip=1024 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/pow"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/dg1ver1.j14"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/dg2ver1.l14"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/dg8.e25" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/dg9.l25" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/dg10.m25" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.11a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.15a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.12a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.16a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=4|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.13a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=4|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.17a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=6|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.14a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=6|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.18a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=8|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.19a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=8|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.23a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=10|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.20a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=10|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.24a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=12|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.21a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=12|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.25a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=14|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.22a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=14|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/snk880.26a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/dg7.d20" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.plds" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/pal20l10.a6" bs=204 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/pow/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/powj"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/1-2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/2-2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/dg8.e25" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/dg9.l25" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/dg10.m25" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.11a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.15a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.12a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.16a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=4|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.13a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=4|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.17a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=6|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.14a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=6|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.18a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=8|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.19a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=8|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.23a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=10|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.20a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=10|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.24a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=12|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.21a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=12|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.25a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=14|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.22a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.gfx2" bs=131072 count=2 skip=14|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/snk880.26a"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/dg7.d20" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.plds" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/pal20l10.a6" bs=204 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/powj.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/powj/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.w.68k" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/gt-e2.2h"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.w.68k" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/gt-e3.3h"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/gt1.1" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.w.alpha.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/gt15.b15" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/pi8914.b14" bs=262144 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.fg.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/pi8916.h16" bs=262144 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.sprites.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/pi8910.k14" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.sprites.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/gt5.5" bs=131072 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.map" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/gt11.11" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.samples" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/gt4.4" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.u.68k" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/gt-u2.2h"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.u.68k" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/gt-u3.3h"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/gt1.1" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.w.alpha.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/gt15.b15" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/pi8914.b14" bs=262144 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.fg.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/pi8916.h16" bs=262144 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.sprites.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/pi8910.k14" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.sprites.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/gt5.5" bs=131072 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.map" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/gt11.11" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.samples" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/gt4.4" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.j.68k" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/gt-j2.2h"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.j.68k" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/gt-j3.3h"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/gt1.1" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.w.alpha.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/gt15.b15" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/pi8914.b14" bs=262144 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.fg.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/pi8916.h16" bs=262144 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.sprites.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/pi8910.k14" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.sprites.rom" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/gt5.5" bs=131072 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.bg.map" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/gt11.11" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/PrehistoricIsleIn1930.samples" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/gt4.4" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc1" bs=2048 skip=0 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc2" bs=2048 skip=1 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc3" bs=2048 skip=2 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc4" bs=2048 skip=3 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc5" bs=2048 skip=4 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc6" bs=2048 skip=5 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc7" bs=2048 skip=6 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc8" bs=2048 skip=7 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc9" bs=2048 skip=8 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc10" bs=2048 skip=9 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/mcs_c" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/mcs_d" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sasuke.clr" bs=32 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/sc11" bs=2048 skip=0 iflag=skip_bytes count=1
	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bhw.2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bhw.3"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.user1" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bhw.1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.user1" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bhw.4"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.5" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.7" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.8" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.c1" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.c3" bs=524288 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.c5" bs=524288 skip=1048576 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.c2" bs=524288 skip=2097152 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.c4" bs=524288 skip=2621440 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.c6" bs=524288 skip=3145728 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/bh.v1" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.3"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.user1" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.user1" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.4"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.5" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.7" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.8" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.c1" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.c3" bs=524288 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.c5" bs=524288 skip=1048576 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.c2" bs=524288 skip=2097152 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.c4" bs=524288 skip=2621440 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.c6" bs=524288 skip=3145728 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/bh.v1" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh2ver3j.9c"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh3ver3j.10c"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.user1" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bhw.1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.user1" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bhw.4"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.5" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.7" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.8" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.c1" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.c3" bs=524288 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.c5" bs=524288 skip=1048576 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.c2" bs=524288 skip=2097152 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.c4" bs=524288 skip=2621440 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.c6" bs=524288 skip=3145728 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/bh.v1" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/s2-1ver2.14h"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/s2-2ver2.14k"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/s2-5.16c" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/s2-9.25l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/s2-10.25m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/stsmart.900" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/stsmart.902" bs=524288 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/stsmart.904" bs=524288 skip=1048576 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/stsmart.901" bs=524288 skip=2097152 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/stsmart.903" bs=524288 skip=2621440 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/stsmart.905" bs=524288 skip=3145728 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/s2-6.18d" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/s2-1ver1.9c"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/s2-2ver1.10c"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/s2-5.16c" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/s2-7.15l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/s2-8.15m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/stsmart.900" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/stsmart.902" bs=524288 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/stsmart.904" bs=524288 skip=1048576 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/stsmart.901" bs=524288 skip=2097152 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/stsmart.903" bs=524288 skip=2621440 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/stsmart.905" bs=524288 skip=3145728 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/s2-6.18d" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/s-smart1.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/s-smart2.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/s2-5.16c" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/s2-7.15l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/s2-8.15m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/stsmart.900" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/stsmart.902" bs=524288 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/stsmart.904" bs=524288 skip=1048576 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/stsmart.901" bs=524288 skip=2097152 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/stsmart.903" bs=524288 skip=2621440 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/stsmart.905" bs=524288 skip=3145728 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/s2-6.18d" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj.maincpu" bs=131072 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/s2v1j_01.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj.maincpu" bs=131072 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/s2v1j_02.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.soundcpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/s2-5.16c" bs=65536 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/s2-7.15l" bs=32768 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/s2-8.15m" bs=32768 skip=32768 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/stsmart.900" bs=524288 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/stsmart.902" bs=524288 skip=524288 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/stsmart.904" bs=524288 skip=1048576 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/stsmart.901" bs=524288 skip=2097152 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/stsmart.903" bs=524288 skip=2621440 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.gfx2" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/stsmart.905" bs=524288 skip=3145728 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.upd" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/s2-6.18d" bs=131072 skip=0 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.3.68k" bs=65536 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.3"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.3.68k" bs=65536 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.4"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.3.68k" bs=65536 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.3.68k" bs=65536 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.7" bs=65536 skip=0 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.8" bs=65536 skip=1 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.9" bs=65536 skip=2 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.u.gfx1.rom" bs=32768 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.6"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.u.gfx1.rom" bs=32768 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.5"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=0 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=4 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.11"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=8 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=12 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.13"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=1 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.14"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=5 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.15"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=9 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.16"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=13 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.17"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=2 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.18"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=6 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.19"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=10 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.20"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=14 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/bf.21"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.68k" bs=65536 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bfv1_03.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.68k" bs=65536 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bfv1_04.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.68k" bs=65536 count=2 skip=2|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.1"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.68k" bs=65536 count=2 skip=2|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.2"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.7" bs=65536 skip=0 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.8" bs=65536 skip=1 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.z80" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.9" bs=65536 skip=2 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.gfx1.rom" bs=32768 count=2 skip=0|load16_byte 1 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bfv1_06.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.j.gfx1.rom" bs=32768 count=2 skip=0|load16_byte 2 >"$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bfv1_05.bin"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=0 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.10"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=4 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.11"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=8 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.12"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=12 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.13"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=1 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.14"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=5 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.15"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=9 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.16"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=13 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.17"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=2 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.18"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=6 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.19"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=10 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.20"
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/TimeSoldiers.gfx2.rom" bs=131072 count=1 skip=14 of="$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/bf.21"

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic07.bin" bs=4096 skip=0 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic08.bin" bs=4096 skip=1 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic09.bin" bs=4096 skip=2 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic10.bin" bs=4096 skip=3 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic13.bin" bs=4096 skip=4 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic14.bin" bs=4096 skip=5 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic15.bin" bs=4096 skip=6 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic16.bin" bs=4096 skip=7 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk5_ic50.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk5_ic51.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk5_ic7.bin" bs=32 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk5_ic6.bin" bs=32 skip=32 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic51.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk4_ic52.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/hd38882.bin" bs=16384 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk6_ic07.bin" bs=2048 skip=16384 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk6_ic08.bin" bs=2048 skip=18432 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/sk6_ic11.bin" bs=2048 skip=20480 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk4_ic07.bin" bs=4096 skip=0 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk4_ic08.bin" bs=4096 skip=1 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk4_ic09.bin" bs=4096 skip=2 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/4" bs=4096 skip=3 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/5" bs=4096 skip=4 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk4_ic14.bin" bs=4096 skip=5 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk4_ic15.bin" bs=4096 skip=6 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/8" bs=4096 skip=7 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk5_ic50.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk5_ic51.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk5_ic7.bin" bs=32 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk5_ic6.bin" bs=32 skip=32 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk4_ic51.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk4_ic52.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/hd38882.bin" bs=16384 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk6_ic07.bin" bs=2048 skip=16384 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk6_ic08.bin" bs=2048 skip=18432 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/sk6_ic11.bin" bs=2048 skip=20480 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc/"* > /dev/null

	mkdir -p "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj"

	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic07.bin" bs=4096 skip=0 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic08.bin" bs=4096 skip=1 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic09.bin" bs=4096 skip=2 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/vgj4ic10.bin" bs=4096 skip=3 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/vgj5ic13.bin" bs=4096 skip=4 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic14.bin" bs=4096 skip=5 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic15.bin" bs=4096 skip=6 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.maincpu" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic16.bin" bs=4096 skip=7 count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk5_ic50.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.gfx1" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk5_ic51.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk5_ic7.bin" bs=32 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.proms" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk5_ic6.bin" bs=32 skip=32 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic51.bin" bs=2048 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.snk6502" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk4_ic52.bin" bs=2048 skip=2048 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/hd38882.bin" bs=16384 skip=0 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk6_ic07.bin" bs=2048 skip=16384 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk6_ic08.bin" bs=2048 skip=18432 iflag=skip_bytes count=1
	dd status=none if="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.speech" of="$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/sk6_ic11.bin" bs=2048 skip=20480 iflag=skip_bytes count=1

	zip -j "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.zip" "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj/"* > /dev/null

	echo "Copy MAME ROMs ..."

	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbusters.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersu.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/bbustersj.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/chopperb.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/legofair.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyu.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/fantasyj.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3u.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3j.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/ikari3k.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/joyfulr.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/mnchmobl.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/ozmawars.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/pow.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/powj.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisle.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/prehisleu.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/gensitou.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/sasuke.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/searchar.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharu.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/searcharj.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsm1.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmw.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/streetsmj.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/timesold.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/btlfield.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguard.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardc.zip" "$SCRIPTID/"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/vanguardj.zip" "$SCRIPTID/"

	echo "Copy NES ROMs .."

	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/ASO.nes" "$SCRIPTID/Alpha Mission (USA).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/ASO_jp.nes" "$SCRIPTID/ASO - Armored Scrum Object (Japan) (En).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Athena.nes" "$SCRIPTID/Athena (USA).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Athena_jp.nes" "$SCRIPTID/Athena (Japan).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Crystalis.nes" "$SCRIPTID/Crystalis (SNK 40th Anniversary Collection) (Unverified).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Crystalis_jp.nes" "$SCRIPTID/Crystalis (Japan) (SNK 40th Anniversary Collection) (Unverified).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/GuerrillaWar.nes" "$SCRIPTID/Guerrilla War (USA).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/GuerrillaWar_jp.nes" "$SCRIPTID/Guevara (Japan).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari.nes" "$SCRIPTID/Ikari Warriors (USA) (Rev 1).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari2.nes" "$SCRIPTID/Ikari Warriors II - Victory Road (USA).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari2_jp.nes" "$SCRIPTID/Ikari II - Dogosoken (Japan).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari3.nes" "$SCRIPTID/Ikari III - The Rescue (USA).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari3_jp.nes" "$SCRIPTID/Ikari III (Japan).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/Ikari_jp.nes" "$SCRIPTID/Ikari (Japan).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/IronTank.nes" "$SCRIPTID/Iron Tank - The Invasion of Normandy (USA).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/IronTank_jp.nes" "$SCRIPTID/Iron Tank (Japan) (SNK 40th Anniversary Collection) (Unverified).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/POW.nes" "$SCRIPTID/P.O.W. - Prisoners of War (USA).nes"
	cp "$ROMEXTRACT_TMPDIR/$SCRIPTID/POW_jp.nes" "$SCRIPTID/Datsugoku (Japan).nes"

	echo "Cleaning up ..."
	rm -r "${ROMEXTRACT_TMPDIR:?}/$SCRIPTID"

	echo "Script $SCRIPTID.sh done"

}
